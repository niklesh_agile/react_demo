

class Welcome extends React.Component {
  render() {
    return <h1>Welcome Component, {this.props.name}</h1>;
  }
}

function makeUpper(str){
	return str.toUpperCase();
}
class Clock extends React.Component{
	constructor(prop){
		super(prop);
		this.state = {date: new Date(),timerID:'',counter:10};
		this.handleClick = this.handleClick.bind(this);
	}
	componentDidMount(){
		this.timerID = setInterval(
				() => this.tick(),
				1000
			);
	}
	componentWillUnmount(){
		clearInterval(this.timerID);
	}
	tick(){
		this.setState({
			date: new Date()
		})
	}
	handleClick(id) {
		console.log(id);
	    this.setState((state, props) => ({
		  counter: parseInt(state.counter) + parseInt(props.increment)
		}));
    }
	render(){
		return <div>
			{this.state.date.toLocaleString()}
			<div>Counter :{this.state.counter}</div>
			<button onClick={this.handleClick.bind(this,1)}>Click Me!</button>
		</div>
	}
}
var gVar = 'Niklesh Raut'
function App(props){
	if(gVar){
		return <div>
			<h1 className='text-success'>Hello {makeUpper(gVar)}</h1>
			<Clock increment="5" />
			<Welcome name={props.name} />
		</div>
	}
	return <h1>Hello Stranger</h1>
}

ReactDOM.render(<App name="User1"/>, document.getElementById('root'))
